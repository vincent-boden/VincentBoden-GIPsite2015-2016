+++
date = "2015-12-07"
draft = false
title = "Frans"
+++

<div style="text-align: center">

# **Français**

<br>

#### **Le FAQ**

Quand vouz cliquez ici vous voyez la FAQ de OneDrive en français.

<a class="btn-floating btn-large waves-effect waves-light" href="../Faq">GO</a>

<br>

#### **Reportage photo**

Quand vouz cliquez ici vous voyez un reportage photo concernant le stage.

<a class="btn-floating btn-large waves-effect waves-light" href="../reportagephoto">GO</a>

</div><br>