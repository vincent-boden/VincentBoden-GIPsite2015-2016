+++
date = "2015-12-07"
draft = false
title = "IT-TAPA"

+++

<div style="text-align: center">

### **TAAK 01: IT-TAPA SESSIE**

<br>

De <b>IT-TAPA middagsessies</b> waren heel goed verlopen. We hebben alles vlot kunnen uitleggen met af en toe een pauze ertussen om de leerkrachten te kunnen laten volgen en om hen de kans te geven om vragen te stellen. Ik vond het zelf heel leuk om te doen omdat we ook veel over het onderwerp <a href="http://www.onedrive.live.com/">OneDrive voor bedrijven</a> en <a href="https://products.office.com/nl-be/business/office">Office 365</a> weten. Het deed me daarbij ook plezier om andere er iets over bij te kunnen leren.

<br>

<a href="http://www.onedrive.live.com/">OneDrive voor bedrijven</a> is een <b>cloudopslag</b> waar u al uw bestanden kan opslaan. Het is speciaal ontworpen om eenvoudig in gebruik te nemen in uw bedrijf. De online toepassingen vergemakkelijken het aanpassen van de bestanden. Zo kan u offline maar ook online uw documenten bewerken. Ook zoals bij OneDrive hebt u de mogelijkheid om uw bestanden te delen en er tezamen aan te werken. <a href="https://products.office.com/nl-be/business/office">Office 365</a> omvat al de verschillende <b>toepassingen van Microsoft Office</b>. Enkele voorbeelden: OneDrive, Word, Excel, Lync, Publisher, ....

<br>

De IT-TAPA middagsessie over OneDrive voor bedrijven en Office 365 is medemogelijk gemaakt door <a href="http://jensvl-immalle.github.io/">Jens Van Loon</a>.

<img width=75%   src="https://scontent-fra3-1.xx.fbcdn.net/hphotos-xat1/t31.0-8/12244273_931268693576811_5501266098225052475_o.jpg"/>

<a class="btn-floating btn-large waves-effect waves-light" href="../IT"><i class="mdi-navigation-arrow-back"></i></a>

</div><br>