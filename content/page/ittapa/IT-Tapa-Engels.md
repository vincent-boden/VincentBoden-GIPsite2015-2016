+++
date = "2015-12-07"
draft = false
title = "TAAK 03 IT-TAPA ENG VIDEO"
+++

<div style="text-align: center">

### **TAAK 03: IT-TAPA INSTRUCTION VIDEO THE ENGLISH WAY**

<br>

<iframe width="75%" height="500" src="https://www.youtube.com/embed/g0GjSCenFNs" frameborder="0" allowfullscreen></iframe>

<a class="btn-floating btn-large waves-effect waves-light" href="../IT"><i class="mdi-navigation-arrow-back"></i></a>

</div><br>

<style>
@media screen and (max-width : 750px){ 
    iframe {
        width: 100%;
        height: auto;
    }
}
</style>