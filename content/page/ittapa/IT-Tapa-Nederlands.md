+++
date = "2015-11-30"
draft = false
title = "IT-TAPA"

+++

<div style="text-align: center">

### **TAAK 02: IT-TAPA INSTRUCTIE FILMPJE NEDERLANDS**

<br>

<iframe width="75%" height="500px" src="https://www.youtube.com/embed/2NbMCpBh0x8" frameborder="0" allowfullscreen></iframe>

<a class="btn-floating btn-large waves-effect waves-light" href="../IT"><i class="mdi-navigation-arrow-back"></i></a>

</div><br>

<style>
@media screen and (max-width : 750px){ 
    iframe {
        width: 100%;
        height: auto;
    }
}
</style>