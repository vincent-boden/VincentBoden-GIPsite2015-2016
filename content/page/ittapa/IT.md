+++
date = "2015-01-01T17:14:11+02:00"
draft = false
title = "IT-TAPA"

+++

<div style="text-align: center">

# **IT-TAPA - teacher Aid**

<br>

#### **IT-TAPA - Sessie**

Als u hier klikt krijgt u meer informatie over de IT-TAPA sessies van OneDrive zelf.

<a class="btn-floating btn-large waves-effect waves-light" href="../IT-Tapa-Sessie">GO</a>

<br>

#### **IT-TAPA - Instructiefilmpje in het Nederlands**

Als u hier klikt ziet u het Nederlandse instructiefilmpje over OneDrive.

<a class="btn-floating btn-large waves-effect waves-light" href="../IT-Tapa-Nederlands">GO</a>

<br>

#### **IT-TAPA - Instructiefilmpje in het Engels**

Als u hier klikt ziet u het Engelse instructiefilmpje over OneDrive.

<a class="btn-floating btn-large waves-effect waves-light" href="../IT-Tapa-Engels">GO</a>

</div><br>