+++
date = "2015-01-01T17:14:11+02:00"
draft = false
title = "Systeembeheer"

+++

<div style="text-align: center">

# **Systeem beheer**

<br>

#### **Case study - Linux op het stagebedrijf**

Als u hier klikt ziet u een vergelijking van de workflows, namelijk Hugo VS Wordpress.

<a class="btn-floating btn-large waves-effect waves-light" href="../casestudy">GO</a>

<br>

#### **Test opstelling - Raspberry Pi met NFC module**

Als u hier klikt ziet u de test opstelling van mij en yorick, namelijk de Raspberry Pi die met de NFC module.

<a class="btn-floating btn-large waves-effect waves-light" href="../testopstelling">GO</a>

</div><br>