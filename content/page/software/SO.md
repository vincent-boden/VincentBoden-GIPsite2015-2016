+++
date = "2015-12-07"
draft = false
title = "Software Ontwikkeling"
+++

<div style="text-align: center">

### **Software Ontwikkeling**

<br>

#### **Vergelijking workflow: Hugo VS Wordpress**

Als u hier klikt ziet u een vergelijking van de workflows, namelijk Hugo VS Wordpress.

<a class="btn-floating btn-large waves-effect waves-light" href="../VergelijkingWorkflows/">GO</a>

<br>

#### **De directory structuur van mijn website**

Als u hier klikt ziet u de directory structuur van deze website. Hoe hij is opgebouwd en waar alles staat.

<a class="btn-floating btn-large waves-effect waves-light" href="../DirectoryStructuur/">GO</a>

<br>

#### **Het creëren van een Database - Chatroom**

Als u hier klikt ziet u de verschillende relaties tussen van de Chatroom database.

<a class="btn-floating btn-large waves-effect waves-light" href="../databasechatroom">GO</a>

<br>

#### **Wetenschappelijke website - interactieve javascript opdracht**

Als u hier op klikt ziet u de uitleg over mijn interactieve javascript opdracht en een link naar deze opdracht.

<a class="btn-floating btn-large waves-effect waves-light" href="../interactievejsapplicatie">GO</a>

<br>

#### **IT-Profile**

Als u hier op klikt ziet u mijn IT-Profile, een procentuele weergaven van hoe mijn IT-skills zijn gevordert t.o.v. het begin van dit schooljaar.

<a class="btn-floating btn-large waves-effect waves-light" href="../itprofile">GO</a>

</div><br>