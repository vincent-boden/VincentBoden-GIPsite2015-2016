+++
date = "2016-04-18"
draft = false
title = "Software Ontwikkeling"
+++

<div style="text-align: center">
### **TAAK 08: Database Chatroom**

<img style="width: 75%;" src="../../../images/taak08.jpg" />

Als u  <a href="../../../images/taak8.sql" download>hier</a> klikt kan u de sql code downloaden die bij het diagramma van hierboven hoort. 

<a class="btn-floating btn-large waves-effect waves-light" href="../SO"><i class="mdi-navigation-arrow-back"></i></a>

</div><br>
