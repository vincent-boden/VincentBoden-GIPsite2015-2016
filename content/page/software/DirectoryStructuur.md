+++
date = "2015-11-23"
draft = false
title = "Software Ontwikkeling"

+++

<div style="text-align: center">
### **Een overzicht van de directory-structuur**

<a href="https://gitlab.com/vincent-boden/VincentBoden-GIPsite2015-2016.git">**GitLab**</a>

<a href="https://github.com/Vincent-Boden/vincent-boden.github.io.git">**GitHub**</a>

<a href="http://vincent-boden.github.io/">**Publicatie URL**</a>

</div><br>

#### Archetypes    

**default.md:** Dit bestand is gekopieerd uit mijn hugo thema maar ik heb het niet gebruikt in mijn site.

<br>

#### Layouts   

**content.html:** Hier staat in **HTML** en **GO** de vorm geschreven hoe de content
map op de site wordt weergegeven, in mijn geval in de vorm van kaartjes.
Ik heb hier een heel kleine aanpassing aangedaan wat ik op zich wel zeer
moeilijk vond omdat ik **GO** niet ken.

**footer.html:** Hierin staat het **pagina einde** van de site, ik heb hier enkel kleine
aanpassingen aan gedaan zoals de school vermelden, achtergrond
aanpassen, … . Hier heb ik geen problemen mee ondervonden.

**header.html:** Hierin staat de **header** van de site en de navigatiebar. Vooral
met de navigatiebar heb ik veel problemen gehad. Ik heb nieuwe pagina’s
aangemaakt, voor elk vak een. Met het toevoegen en linken in de navigatiebar
heb ik **enkele problemen** ondervonden, de pagina’s bleven namelijk ook tussen
de posts staan wat dus niet de bedoeling is.

**pagination.html:** Dit is een belangrijk bestand voor maken van **meerdere post
pagina’s** en het navigeren daarheen. Ik ben hier dus vanaf afgebleven.

**index.html:** Dit is de verdeling van waar al de **HTML** zal worden geplaatst als
het in de public mat terecht komt.

<br>

#### Static        

**style.css:** Hier heb ik de **CSS** van mijn thema in gekopieerd en een beetje
aangepast naar hoe ik het wilde. Het moeilijke hierbij was dat ik eerst niet goed
wist wat bij welk hoorde op de site dus moest ik dit eerst enkele keren **testen**
voor dat het goed werkte.

**materialize.min.js:** Hier staat al de **Javascript** van mijn thema. Ik heb hier niets
in aangepast omdat ik vind dat de **Javascript** van mijn thema er goed uit ziet.

<br>

#### Content

**Al de content:** Hier staan alle **pages en posts** in. Deze map stond standaard in het **hugo**    
bestand. De inhoud heb ik volledig zelf gemaakt met behulp van de **command
prompt**. Hier waren geen moeilijkheden bij.

<br>

#### Rest

**config.toml:** In dit bestand staat de **configuratie** van jou thema, ik heb hier veel
in aangepast gedurende de vordering van de site omdat ik soms niet goed
wist wat hier allemaal moest staan.


<div style="text-align: center">
<a class="btn-floating btn-large waves-effect waves-light" href="../SO"><i class="mdi-navigation-arrow-back"></i></a>
</div><br>
