+++
date = "2015-11-23"
draft = false
title = "Software Ontwikkeling"

+++

<div style="text-align: center">
### **TAAK 07: VERGELIJKING HUGO - WORDPRESS**

<br>

Hugo is een zeer handige software om je eigen **blog/website** mee te maken. Het nadeel bij **Hugo** is dat
het heel moeilijk is om ermee te starten. Als je niet weet hoe Hugo werkt moet je eerst heel veel testen,
onderzoeken en proberen te werken met verschillende thema’s. Elk thema heeft namelijk een **aparte manier
van configureren** dus je moet eerst een thema vinden dat je zou willen gebruiken en dan moet je de "*README.md*"
lezen of extra uitleg zoeken op de Hugo site zelf om de configuratie van dit bepaalde thema onder de knie
te krijgen. Vanaf dat je je thema volledig hebt geconfigureerd en hebt aangepast naar jouw voorkeur is het
toevoegen van **posts** wel handig en eenvoudig. Jouw bepaalde thema geeft een bepaalde opmaak aan jou **markdown**
post en het schrijven van een markdown post is vrij eenvoudig als je eventjes onderzoek doet.

<br>

Ik heb ongeveer dezelfde posts nagemaakt met dezelfde titels op **Wordpress** en dit zijn mijn conclusies in
vergelijking met het gebruik van Hugo. Wordpress werkt zeer eenvoudig zonder dat je al te veel moet
onderzoeken kan je **vrijwel meteen een site/blog starten** en posts beginnen plaatsen. Er zijn ook hier
verschillende thema’s waar je uit kan kiezen maar deze werken wel allemaal hetzelfde, je hoeft dus **geen
apart onderzoek** te doen voor elk thema. Het **opmaken** van een post in Wordpress werkt zeer eenvoudig,
je hebt een opmaak lint met allerlei opmaak toepassingen. Je kan ook zeer gemakkelijk afbeeldingen,
locaties, … toevoegen aan je post.

<br>

Het **publiceren** van de Wordpress blog is zeer handig, het werkt
meteen als je de blog hebt aangemaakt en wanneer je je e-mail hebt bevestigd. Het publiceren via **GitHub**
met Hugo daarentegen brengt wel enkele problemen met zich mee. De **baseUrl** wou nooit aanpassen en bleef
steeds op "*localhost:1313*" staan, wat er dus voor zorgde dat de site niet kon laden. Soms wilde de CSS ook niet
laden en kon ik enkel de inhoud zien van mijn site zonder opmaak. Maar wanneer je eenmaal weet hoe het
werkt gaat het wel vlot. Naar mijn eigen mening vind ik Wordpress wel **handiger** om mee te werken maar ik
vind Hugo beter omdat je hier zelf meer moeite in moet steken en dat geeft een **gevoel van voldoening** dat
je toch moeite hebt gestoken in het maken van je site/blog.

<a class="btn-floating btn-large waves-effect waves-light" href="../SO"><i class="mdi-navigation-arrow-back"></i></a>
</div><br>