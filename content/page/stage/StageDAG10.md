+++
date = "2016-01-22"
draft = false
title = "Software Ontwikkeling"

+++

<div style="text-align: center">

### Stageverslag - dag 10

<img style="width: 50%; height: 50%" src="/images/FotoDag10.png" />

Op stage dag tien hebben we niet zo veel gedaan. We hebben eerst de laatste laadkades voorzien van de nieuwe software en daarna was mijn klastitularis gearriveerd om een kleine bespreking te doen met de twee stagebegeleiders. Daarna ben ik er zelf ook nog even bij gekomen om mijn ervaring over de stage met hen te delen.

In de namiddag heb ik samen met mijn stagebegeleider een Raspberry Pi geconfigureerd die ik achteraf zelf mee naar huis mocht nemen. Dit was een heel aangename verassing en ik mocht daarbij ook nog eens om twee uur al naar huis.

<br>

Ik vond de stage zelf een heel leerrijke ervaring, ook de mensen waar ik mee samenwerkte waren stuk voor stuk fantastische mensen die mij graag wilde helpen en uitleg wilde geven over hoe zij werkte. Ze hebben mij zelfs vakantiewerk aangeboden voor in de grote vakantie, ik ben van plan om dit zeker te doen omdat dit een grote kans voor mij is die ik niet kan laten liggen.

<a class="btn-floating btn-large waves-effect waves-light" href="../ST/"><i class="mdi-navigation-arrow-back"></i></a>

</div>
<br>
