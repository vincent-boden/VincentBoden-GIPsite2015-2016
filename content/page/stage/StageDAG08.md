+++
date = "2016-01-20"
draft = false
title = "Software Ontwikkeling"

+++

<div style="text-align: center">

### Stageverslag - dag 8

<img style="width: 50%; height: 50%" src="/images/FotoDag08.png" />

Ook op stage dag acht hebben we de software vervangen van een van de Smartboxes die aan de laadkades gekoppeld zijn vervangen. Hierbij was er weer iets misgegaan. Omdat de Raspberry Pi die in de Smartbox aanwezig is iets te dicht bij een andere component van de Smartbox zat zijn deze deeltjes tegen elkaar gekomen bij het insteken van de nieuwe SD-kaart met de nieuwe software en dit heeft kortsluiting veroorzaakt waardoor de Raspberry Pi stuk was en vervangen moest worden.

De namiddag heb ik helemaal gespendeerd aan het maken van verschillende scriptjes in opdracht van mijn stagebegeleider. Ook heb ik zelf enkele script ideeën bedacht en uitgewerkt.

<a class="btn-floating btn-large waves-effect waves-light" href="../ST/"><i class="mdi-navigation-arrow-back"></i></a>

</div>
<br>
