+++
date = "2016-01-14"
draft = false
title = "Software Ontwikkeling"

+++

<div style="text-align: center">

### Stageverslag - dag 4

<br>

<img style="width: 50%; height: 50%" src="/images/FotoDag04.png" />

Op de vierde stage dag heeft mijn stagebegeleider mij aan het werk gezet met als taak dat ik verschillende computers moest imagen en de nodige software erop moest installeren. Dit was nodig omdat enkele mensen in het bedrijf een nieuwe computer nodig hadden en er moesten ook enkele correct ingestelde pc's in de opslag plaats gezet worden voor het geval dat een computer het begeeft. Zo is er meteen vervanging voor die persoon en zo beschikt men altijd over een computer anders kan men niet verder werken natuurlijk.

<br>

Na dit hebben we ook nog enkele interne issues in het bedrijf opgelost. Omdat het imagen zolang duurde hebben we vandaag geen verschillende taken kunnen uitvoeren.

<br>

<a class="btn-floating btn-large waves-effect waves-light" href="../ST/"><i class="mdi-navigation-arrow-back"></i></a>

</div>
<br>
