+++
date = "2016-01-19"
draft = false
title = "Software Ontwikkeling"

+++

<div style="text-align: center">

### Stageverslag - dag 7

<img style="width: 50%; height: 50%" src="/images/FotoDag07.png" />

Op stage dag zeven zijn we gestart met de software van de volgende Smartbox die aan deze laadkade gekoppeld is te vervangen zoals u in de afbeelding kan zien. Bij het vervangen van deze software ging er van alles mis en hier hebben we dan vrij veel tijd ingestoken om achteraf tot de conclusie te komen dat er twee lampen in de lampen toren defect waren.

Daarna heb ik een soort van vergadering mogen bijwonen waar men een nieuw aangekaart idee gaat voorstellen en bespreken wanneer en hoe ze dit gaan doen zodat ik ook eens een beeld zou krijgen van hoe dat in zijn werk gaat.

De laatste twee uur heeft mijn stagebegeleider mij aan het werk gezet met Linux commando's. Ik moest zelf uitzoeken en experimenteren hoe ik moest navigeren, bestanden maken, bewerken, ... in bash. Ook heb ik moeten uitzoeken hoe je scripts in bash kan maken, schrijven en uitvoeren voor eigen gebruik. Na enige tijd zoeken is dit allemaal wel gelukt.

<a class="btn-floating btn-large waves-effect waves-light" href="../ST/"><i class="mdi-navigation-arrow-back"></i></a>

</div>
<br>
