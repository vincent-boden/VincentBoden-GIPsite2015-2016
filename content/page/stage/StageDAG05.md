+++
date = "2016-01-15"
draft = false
title = "Software Ontwikkeling"

+++

<div style="text-align: center">

### Stageverslag - dag 5

<br>

<img style="width: 50%; height: 50%" src="/images/FotoDag05.png" />

Op stage dag vijf ben ik in de voormiddag nog even alleen verder bezig geweest met het imagen van de computers en laptops, er moesten er in totaal 15 gedaan worden dus dit nam heel de voormiddag in. Tussen het imagen van de computers door heb ik de tijd gekregen om aan enkele GIP-opdrachten te werken met een laptop van het bedrijf.

<br>

In de namiddag hebben we de computers uit elkaar gehaald om te controleren dat alles aanwezig is en om eventueel extra geheugen erin te steken voor moest er nog niet genoeg in zitten.

<br>

Daarna heeft mijn stagebegeleider nog een uitleg gegeven over enkele systemen die zij gebruiken in het bedrijf en ook dat wanneer zei iets nieuws gebruiken dat hier altijd een handleiding voor moet geschreven worden voor stel dat de persoon die het gebruikt voor langere tijd ziek is of ontslagen wordt dat iemand de handleiding kan nemen en dat hij of zij er dan meteen mee aan de slag kan.

<br>

<a class="btn-floating btn-large waves-effect waves-light" href="../ST/"><i class="mdi-navigation-arrow-back"></i></a>

</div>
<br>
