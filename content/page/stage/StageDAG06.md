+++
date = "2016-01-18"
draft = false
title = "Software Ontwikkeling"

+++

<div style="text-align: center">

### Stageverslag - dag 6

<img style="width: 50%; height: 50%" src="/images/FotoDag06.png" />

Op stage dag zes heb ik een nieuwe stagebegeleider gekregen. Hij houdt zich vooral bezig met de Smartboxen, dit is een case waarin een Raspberry Pi aanwezig is waarop een eigen geschreven applicatie draait. Dit programma staat op het SD-kaartje dat in de Raspberry Pi zit. Voor al de aanwezige Smartboxen aan de laadkades moest de software vervangen worden terwijl er mensen aan het werk waren in de fabriek. We moesten hier soms op wachten om het te testen en ook totdat ze weg waren zodat wij de tijd kregen om het te installeren.

De bedoeling is dat we tegen het einde van de week al de Smartboxen bij de laadkades voorzien hebben van de nieuwe Software.

<br>

De nieuwe stagebegeleider heeft ook een gedetailleerde uitleg gegeven over Linux in het algemeen, hier ga ik ook mijn Case Study over doen. Hij heeft uitgelegd waar het vandaan komt, waarom ze het op NV Imas gebruiken en hij heeft mij enkele commando's laten uittesten. In de loop van deze week is het de bedoeling dat ik vlot kan werken met de command line in Linux, hierbij aan gekoppeld maak ik gebruik van de command line via een Raspberry Pi.

<a class="btn-floating btn-large waves-effect waves-light" href="../ST/"><i class="mdi-navigation-arrow-back"></i></a>

</div>
<br>
