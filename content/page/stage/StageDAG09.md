+++
date = "2016-01-21"
draft = false
title = "Software Ontwikkeling"

+++

<div style="text-align: center">

### Stageverslag - dag 9

<img style="width: 90%" src="/images/FotoDag09.png" />

Op stage dag negen heeft mijn stagebegeleider mij twee grote opdrachten gegeven, namelijk leren werken met het ssh commando over heel het domein van Nv Imas en een uitgebreid script schrijven dat automatisch alles wegschreef in een log bestand door middel van functies. Het wegschrijven in een log bestand van de gebeurtenissen is iets heel belangrijk. Als er iets misloopt kan je door de logfiles te bekijken meteen zien wat er fout is gegaan en zo voorkom je lang zoeken naar iets klein wat je heel eenvoudig kunt oplossen met een log functie te schrijven.

<br>

We wilde ook nog de software vervangen van een volgend laadkade maar er was iets misgegaan met de Smartbox van de laadkade die we gisteren hadden vervangen. Er kwam namelijk een fout aantal volt binnen waardoor de Raspberry Pi niet kon werken. Dit heeft mijn stagebegeleider opgelost terwijl ik met de scripts bezig was anders nam dit te veel tijd in beslag, zo kon ik meer met scripting bezig zijn.

<a class="btn-floating btn-large waves-effect waves-light" href="../ST/"><i class="mdi-navigation-arrow-back"></i></a>

</div>
<br>
