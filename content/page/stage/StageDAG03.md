+++
date = "2016-01-13"
draft = false
title = "Software Ontwikkeling"

+++

<div style="text-align: center">

### Stageverslag - dag 3

<br>

Op stage dag drie zijn we vooral bezig geweest met het oplossen van interne issues van de werknemers in het bedrijf. Naast het uitleggen van hoe het netwerk van Imas eruit ziet is het natuurlijk ook nog de bedoeling dat het normale werk van mijn begeleider vooruit geraakt daarom is dit zowat het enige naast enkele kleine taakjes waar we ons vandaag mee bezig hebben gehouden.

<br>

Ik vond sommige van deze taken een beetje saaier maar ik weet dat dit er ook bij hoort dus op zich vond ik dit niet zo erg. Bij het oplossen van deze issues bij de werknemers kom je ook veel in contact met andere mensen dit vind ik persoonlijk zeer fijn, zo kom je te weten met wie je allemaal samenwerkt en wie welke functie heeft in het bedrijf.

<br>

<a class="btn-floating btn-large waves-effect waves-light" href="../ST/"><i class="mdi-navigation-arrow-back"></i></a>

</div>
<br>
