+++
date = "2016-01-11"
draft = false
title = "Software Ontwikkeling"

+++

<div style="text-align: center">

### Stageverslag - dag 1

<br>

<img style="height: 50%; width: 50%" src="/images/FotoDag01.png" />

Vandaag heb ik bij de aankomst op het stagebedrijf een rondleiding gekregen door het bedrijf en door de fabriek. Daarna zijn we meteen begonnen met het installeren van OS's op enkele computers. In de namiddag heeft men uitleg gegeven over hoe het netwerk in elkaar zit en de redenen waarom ze grotendeels Linux Ubuntu gebruiken in plaats van Windows. Men heeft ook uitleg gegeven over de werking van enkele hardware matige apparaten. Tussen deze opdrachten en uitleg door werden we soms opgebeld om iets IT-gerelateerd op te gaan lossen, hier mocht ik met mee gaan om te zien hoe de trouble shooting werkte en hoe de problemen werden opgelost.

<br>

Ik vond de allereerste dag een zeer leerrijke dag. Ik heb veel geleerd over hoe het nu in het echt eraan toe gaat in verband met een netwerk in een heel groot bedrijf. We hebben deze onderwerpen ook wel gezien in de klas maar vanuit het standpunt van een groot bedrijf zie je uiteindelijk veel meer en ook het feit dat men hier een Linux netwerk heeft geeft je een heel andere kijk t.o.v. een Windows netwerk.

<br>

<a class="btn-floating btn-large waves-effect waves-light" href="../ST/"><i class="mdi-navigation-arrow-back"></i></a>

</div>
<br>
