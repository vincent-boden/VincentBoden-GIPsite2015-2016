+++
date = "2016-01-12"
draft = false
title = "Software Ontwikkeling"

+++

<div style="text-align: center">

### Stageverslag - dag 2

<br>

<img style="height: 50%; width: 50%" src="/images/FotoDag02.png" />

Op mijn tweede stage dag zijn we begonnen met het inpluggen van enkele netwerk kabels omdat er problemen waren met enkele pc's hun internet. Daarna heb ik een uitgebreidere uitleg gekregen over het netwerk, ook over dat van de buitenlandse filialen.

<br>

In de namiddag zijn we grotendeels bezig geweest met het verwijderen, plaatsen en vervangen van een tiental access points die zich in de fabriek bevonden, dit deden we met een hoogtewerker.

<br>

We hebben ook even een kijkje genomen in een van de twee server lokalen zoals u ziet op de afbeelding, hier heeft men wat extra uitleg bij gegeven.

<br>

<a class="btn-floating btn-large waves-effect waves-light" href="../ST/"><i class="mdi-navigation-arrow-back"></i></a>

</div>
<br>
