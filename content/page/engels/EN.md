+++
date = "2015-01-01T17:14:11+02:00"
draft = false
title = "Engels"

+++

<div style="text-align: center">

# **Engels**

<br>

#### **IT-autobiography - Me and IT**

Als u hier klikt ziet u een engelse autobiografie over mijn leven en informaticabeheer

<a class="btn-floating btn-large waves-effect waves-light" href="../itautobiography">GO</a>

<br>

#### **Review of a hardware - The Raspberry Pi**

Als u hier klikt ziet u commentaar over een review die gemaakt is van de Raspberry Pi.

<a class="btn-floating btn-large waves-effect waves-light" href="../reviewofahardware">GO</a>

</div><br>