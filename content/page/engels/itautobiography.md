+++
date = "2015-11-30"
draft = false
title = "Engels"

+++

<div style="text-align: center">

### **IT-autobiography - Me and IT**

<br>

<br>

<img class="rotate90" src="http://vincent-boden.github.io//images/AfbVincent.png" width="320" height="180">

<br>

<br>

##### <b>Introduction</b>

My name is Vincent Boden, a fanatic website builder. I'm 17 years old and I live in <a href="http://www.beerse.be/">Beerse</a>. My school is <a href="http://www.immalle.be/immalle/">Immaculata Institute</a> in Oostmalle where I study IT in my sixth year.

<br>

##### <b>What was my first contact with IT?</b>

My first contact with IT was when I was very young, around the age of six. One of my uncles worked in an **IT-related business** and every time I saw him he told something about what they did. Most of all they made **websites** and I was really interested even when I was that young. When my parents bought my first pc my **addiction** started to grow. First I just started with playing games and visiting information websites from stores in my town but as I got older I started to learn how to use my computer as a **software development tool** with the help of my uncle when he had time. Of course I still play lots of games in my spare time but building websites is also something that I like.

<br>

##### <b>What means IT for me?</b>

IT means a lot for me, every single moment (when I didn't plan anything that day) I take my computer and I play games, build websites, do something for school, … . There is actually **not a single day** that I haven’t used my pc. In the vacations I use my pc even more because at my **holiday job** I work with a pc nine hours a day and when I come home from working I start up my own pc. Sometimes they even ask me **IT-related questions** at my holiday work and it gives a really nice feeling if I can help them with what I know. So I can't actually say how much time I spend on my computer a day because it's always different. I am not an expert at what I do but I developped personally a lot with my **IT skills**. If I look at what I was able to do three years ago and what I can do know … . Three years ago I only knew the **basics** and now I learn something new about computers at school every day.

<br>

##### <b>How do you see the future?</b>

I think when I finish my sixth year at <a href="http://www.immalle.be/immalle/">Immaculate Institute</a> I am going to study something with building websites in Antwerp. Maybe in the course of this school year I am going to follow a few lessons about **JavaScript**, **HTML** and **CSS** at <a href="http://www.kdg.be/opleidingen/">Karel de Grote high school</a>. If I pass the exam I don’t have to study those lessons next year anymore and the things I learn there could always come in handy this school year. After I finish my **bachelor** or **master** degree I hope I can start in a business where they make beautiful, interactive and responsive websites of course so I can fully express **my passion** into my work.

<br>

##### <b>Conclusion</b>

**IT** is as I already mentioned something really important in my life. I can’t imagine a life without my computer, smartphone, … . It is a big part of my life and I really like the fact that my friends of the class share this passion with me so there is always something IT-related to talk about in class.

<br>

<a class="btn-floating btn-large waves-effect waves-light" href="../EN"><i class="mdi-navigation-arrow-back"></i></a>

</div><br>