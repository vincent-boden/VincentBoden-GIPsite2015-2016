+++
date = "2016-04-25"
draft = false
title = "Engels"
+++

<div style="text-align: center">

### **Review of a hardware - The Raspberry Pi**

The Raspberry Pi is actually a really small computer, the most famous cheap circuit board computer. It was made and launched by the Raspberry Pi foundation. Until today they have developed four different versions of the small computer, the Raspberry Pi A, A+, B and B+. Besides the fact that you can use the Raspberry Pi to save your documents , it can be used in countless ways. The Raspberry Pi is a very flexible device.

<img style="width: 75%" src="/images/FotoDag10.png" />

</div>
<br>

#### Reference to the online review:

This is the <a href="http://www.trustedreviews.com/raspberry-pi-2-review">link</a> to the online review of the Raspberry Pi 2 or the Raspberry Pi B+. Overall the review is in favour of the Raspberry Pi because most of the comments are good ones.

#### Pros and cons of the review:

I totally agree with most of the things that the reviewer mentioned in his review because I have a Raspberry Pi at home so I can see and I have proof that it is right or wrong what he says.
 
The Raspberry Pi is a good way to learn something about Linux and working with the Linux terminal. This is totally true because I learned at my training company how to work with Linux and how to use the different commandos. 

You can’t use Windows or Android on your Raspberry Pi yet. This is totally not true, but I think this is because the review is two years old and the option to use Windows and Android on your Raspberry Pi is very new. 

You can use a Raspberry Pi for very varied things (E.G.: controlling your television, setting up your own webserver to host websites, make your own robot, …). I agree with this because at my training company they use Raspberry Pis very often with lots of different things (E.G.: a counter, a scanner, controlling radio, …). 

Sometimes your Raspberry Pi demands some patience. I agree to a certain extent, because this is only when you are doing lots of heavy things at the same time. If you’re only doing one thing on your Raspberry Pi it all runs very smoothly. 

You can switch very easily between different operating systems since you are working with SD cards. The best way to store your data is on a USB-stick if you are working with different operating systems at one Raspberry Pi. I agree to a certain extent, but if you change your SD card too much you can accidentally break something in your Raspberry Pi because it is very small and fragile. 
It is not recommended to use a Raspberry Pi as your main computer at home. I agree with this statement because the Raspberry PI is a beautiful computer which can do very varied things but it is not made to be used as a main computer because a Raspberry Pi is more built for coding, testing, … and this without a graphical user interface. If you don’t know much about computers it is very difficult to work with a computer without a graphical user interface.

#### Conclusion

The Raspberry Pi is a very handy tool, something you must have as an IT person. I highly recommend the Raspberry Pi to everybody who is lazy and wants to automate everything and people who want to have a hobby or a challenge to learn something new.

<br>

<div style="text-align: center">
<a class="btn-floating btn-large waves-effect waves-light" href="../EN"><i class="mdi-navigation-arrow-back"></i></a>
</div><br>