+++
date = "2015-01-01T17:14:11+02:00"
draft = false
title = "FAQ"

+++

<div style="text-align: center">

# **Frequently Asked Questions - FAQ**

<br>

#### **De FAQ in het Nederlands**

Als u hier klikt ziet de FAQ over OneDrive in het Nederlands

<a class="btn-floating btn-large waves-effect waves-light" href="../FAQ Nederlands/">GO</a>

<br>

#### **De FAQ in het Frans**

Quand vouz cliquez ici vous voyez la FAQ de OneDrive en français.

<a class="btn-floating btn-large waves-effect waves-light" href="../FAQ Frans/">GO</a>

</div><br>