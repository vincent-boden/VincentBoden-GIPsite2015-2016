+++
date = "2015-12-14"
draft = false
title = "FAQ"

+++

<style>
    ul li {
        width: 12.5%;
    }
    li {
        width: 100%;
    }
</style>


<div style="text-align: center">

### <b>FAQ in het Nederlands</b>

</div>
<br>

#### <b>Wat is het verschil tussen OneDrive en Dropbox?</b>

Hier ziet u een overzicht van enkele verschillen tussen **OneDrive en Dropbox**:

1. Elk bestandstype kan op beide worden geüpload maar op Dropbox heb je niet de mogelijkheid om bestanden online te bewerken of te openen. Bij OneDrive heb je deze mogelijkheid bij veel bestandstypes wel.

2. Bij Dropbox start je maar met 2 GiB gratis opslag ruimte, bij OneDrive heb je gratis … GiB opslag ruimte. Beide kan je verhogen door betalingen. Bij Dropbox kan je ook extra GiB opslag ruimte verdienen door bijvoorbeeld je Facebook vrienden uit te nodigen om Dropbox te gebruiken.

3. Alleen Dropbox is beschikbaar op Linux apparaten maar Dropbox is dan weer niet beschikbaar op Windows Phones. OneDrive kan men op elk apparaat gebruiken behalve met Linux.

<br>


#### <b>Wat is er verandert in Office 2013 ten opzichte van Office 2007?</b>

Er zijn verschillende punten verandert in Office 2013, hier heeft u een overzicht van de belangrijkste veranderingen:

1. Men heeft ervoor gezorgd dat het aangenamer werken is via je tablet of smartphone bij de nieuwe Office versie. De apps zullen een aangepaste lay-out hebben en tussen de bewerkingsknoppen heeft men meer plaats gelaten voor eenvoudigere interactie met vinger of pen.

2. Office 2013 is enkel maar beschikbaar vanaf Windows 7 of hoger. De interactie tussen Office 2013 en Windows 8 verloopt daardoor perfect.

3. De online Cloud services zijn geoptimaliseerd voor betere prestaties. Het online bewerken van documenten verloopt nu vrijwel zonder problemen.

4. U kunt via Word snel en eenvoudig online foto's/video's toevoegen aan je Word documenten. Er is daarbij ook een eenvoudige optie om dit via je persoonlijke OneDrive te doen.

Alles is dus gebruiksvriendelijker en eenvoudiger gemaakt: sneller, verzorgdere lay-out, meer georganiseerd, … .

<br>

#### <b>Is er een verschil tussen Office online en Office offline?</b>

Ja, er zijn enkele verschillen. Je hebt bijvoorbeeld minder opmaak mogelijkheden bij Office online dan bij Office online. Maar het principe blijft hetzelfde. Er zijn nog altijd wel veel opties ter beschikken in Office online.

<br>

#### <b>Hoe beschermt u uw documenten tegen aanpassingen van andere gebruikers?</b>

Als u een privé document hebt of als u een document hebt dat andere gebruikers mogen lezen maar niet mogen bewerken kunt u altijd uw document beschermen:

1. In de linkerbovenhoek van uw document klikt u op "Bestand". Vervolgens gaat u naar "Info". Hier ziet u het kadertje "Document beveiligen".

2. Als u hierop klikt ziet u een overzicht met de verschillende beveiligingsopties. Nu kan u uw document beveiligen naar hoe u het wilt.

<br>

#### <b>Waar kan u OneDrive overal gebruiken?</b>

OneDrive is een toepassing dat op verschillende platformen werkt. U kan het op zo goed als elk toestel gebruiken. U kan het bijvoorbeeld downloaden op uw computer of het downloaden als een app voor iOS, Android, Windows. Zo heeft u toegang tot al u documenten waar u ook bent.

<br>

#### <b>Hoe kunt u bestanden delen met OneDrive for Business?</b>

Het is heel eenvoudig om documenten te delen via OneDrive for Business. Volg deze stappen en u hebt in enkele minuten een bestand gedeeld:

1. U moet links naast de naam van u document klikken om het document te selecteren dat u wilt delen.

2. Daarna klikt u vanboven op delen.

3. Nu ziet u een menu met verschillende opties voor het delen. U kan hier ook de persoon/personen ingeven waarmee u het bestand wilt delen.

<br>

#### <b>Welke bestandstypes kan u uploaden op OneDrive?</b>

Er zijn twee opties die u kan kiezen voor het uploaden van bestanden in OneDrive online. U kan een nieuw Office document aanmaken zoals Excel, Word, PowerPoint, … Maar u heeft ook een upload mogelijkheid voor alle verschillende bestanden op uw computer, bijvoorbeeld: muziek, films, … . Per account kan je een maximum van 10 GiB uploaden.

<br>

#### <b>Hoe verandert u de extensie van een bestand in Office 365?</b>

In Office hebt u de mogelijkheid om meerdere extensies te gebruiken dan enkel de standaard extensies van Office. Zo kan u bijvoorbeeld heel eenvoudige een Word document veranderen in een PDF document door enkele eenvoudige stappen:

1. Klik in de linkerbovenhoek op "Bestand" en vervolgens op "Opslaan als".

2. Nu kiest u de plaats waar u het bestand wilt opslaan op uw computer.

3. Onder de kader waar u de opslagplaats van het bestand kan kiezen ziet u een kadertje waar u de extensie van het bestand kan bepalen door gewoonweg erop te klikken.

<div style="text-align: center">
<a class="btn-floating btn-large waves-effect waves-light" href="../faq"><i class="mdi-navigation-arrow-back"></i></a>
</div><br>
