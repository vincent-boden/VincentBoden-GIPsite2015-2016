+++
date = "2015-11-23"
draft = false
title = "VERGELIJKING - HUGO & WORDPRESS"

+++

<div style="text-align: center">
### **TAAK 07: VERGELIJKING HUGO - WORDPRESS**

<br>

Hugo is een zeer handige software om je eigen **blog/website** mee te maken. Het nadeel bij **Hugo** is dat
het heel moeilijk is om ermee te starten. Als je niet weet hoe Hugo werkt moet je eerst heel veel testen,
onderzoeken en proberen te werken met verschillende thema’s. Elk thema heeft namelijk een **aparte manier
van configureren** dus je moet eerst een thema vinden dat je zou willen gebruiken en dan moet je de "*README.md*"
lezen of extra uitleg zoeken op de Hugo site zelf om de configuratie van dit bepaalde thema onder de knie
te krijgen. Vanaf dat je je thema volledig hebt geconfigureerd en hebt aangepast naar jouw voorkeur is het
toevoegen van **posts** wel handig en eenvoudig. Jouw bepaalde thema geeft een bepaalde opmaak aan jou **markdown**
post en het schrijven van een markdown post is vrij eenvoudig als je eventjes onderzoek doet.

<br>

Ik heb ongeveer dezelfde posts nagemaakt met dezelfde titels op **Wordpress** en dit zijn mijn conclusies in
vergelijking met het gebruik van Hugo. Wordpress werkt zeer eenvoudig zonder dat je al te veel moet
onderzoeken kan je **vrijwel meteen een site/blog starten** en posts beginnen plaatsen. Er zijn ook hier
verschillende thema’s waar je uit kan kiezen maar deze werken wel allemaal hetzelfde, je hoeft dus **geen
apart onderzoek** te doen voor elk thema. Het **opmaken** van een post in Wordpress werkt zeer eenvoudig,
je hebt een opmaak lint met allerlei opmaak toepassingen. Je kan ook zeer gemakkelijk afbeeldingen,
locaties, … toevoegen aan je post.

<br>

Het **publiceren** van de Wordpress blog is zeer handig, het werkt
meteen als je de blog hebt aangemaakt en wanneer je je e-mail hebt bevestigd. Het publiceren via **GitHub**
met Hugo daarentegen brengt wel enkele problemen met zich mee. De **baseUrl** wou nooit aanpassen en bleef
steeds op "*localhost:1313*" staan, wat er dus voor zorgde dat de site niet kon laden. Soms wilde de CSS ook niet
laden en kon ik enkel de inhoud zien van mijn site zonder opmaak. Maar wanneer je eenmaal weet hoe het
werkt gaat het wel vlot. Naar mijn eigen mening vind ik Wordpress wel **handiger** om mee te werken maar ik
vind Hugo beter omdat je hier zelf meer moeite in moet steken en dat geeft een **gevoel van voldoening** dat
je toch moeite hebt gestoken in het maken van je site/blog.

<br>

---

<br>

### Een overzicht van de directory-structuur

<a href="https://gitlab.com/vincent-boden/VincentBoden-GIPsite2015-2016.git">**GitLab**</a>

<a href="https://github.com/Vincent-Boden/vincent-boden.github.io.git">**GitHub**</a>

<a href="http://vincent-boden.github.io/">**Publicatie URL**</a>

</div>
<br>

##### Archetypes:    

**default.md:** Dit bestand is gekopieerd uit mijn hugo thema maar ik heb het niet gebruikt in mijn site.

<br>

##### Layouts:   

**content.html:** Hier staat in **HTML** en **GO** de vorm geschreven hoe de content
map op de site wordt weergegeven, in mijn geval in de vorm van kaartjes.
Ik heb hier een heel kleine aanpassing aangedaan wat ik op zich wel zeer
moeilijk vond omdat ik **GO** niet ken.

**footer.html:** Hierin staat het **pagina einde** van de site, ik heb hier enkel kleine
aanpassingen aan gedaan zoals de school vermelden, achtergrond
aanpassen, … . Hier heb ik geen problemen mee ondervonden.

**header.html:** Hierin staat de **header** van de site en de navigatiebar. Vooral
met de navigatiebar heb ik veel problemen gehad. Ik heb nieuwe pagina’s
aangemaakt, voor elk vak een. Met het toevoegen en linken in de navigatiebar
heb ik **enkele problemen** ondervonden, de pagina’s bleven namelijk ook tussen
de posts staan wat dus niet de bedoeling is.

**pagination.html:** Dit is een belangrijk bestand voor maken van **meerdere post
pagina’s** en het navigeren daarheen. Ik ben hier dus vanaf afgebleven.

**index.html:** Dit is de verdeling van waar al de **HTML** zal worden geplaatst als
het in de public mat terecht komt.

<br>

##### Static:        

**style.css:** Hier heb ik de **CSS** van mijn thema in gekopieerd en een beetje
aangepast naar hoe ik het wilde. Het moeilijke hierbij was dat ik eerst niet goed
wist wat bij welk hoorde op de site dus moest ik dit eerst enkele keren **testen**
voor dat het goed werkte.

**materialize.min.js:** Hier staat al de **Javascript** van mijn thema. Ik heb hier niets
in aangepast omdat ik vind dat de **Javascript** van mijn thema er goed uit ziet.

<br>

##### Content:    

**Al de content:** Hier staan alle **pages en posts** in. Deze map stond standaard in het **hugo**    
bestand. De inhoud heb ik volledig zelf gemaakt met behulp van de **command
prompt**. Hier waren geen moeilijkheden bij.

<br>

##### Rest:

**config.toml:** In dit bestand staat de **configuratie** van jou thema, ik heb hier veel
in aangepast gedurende de vordering van de site omdat ik soms niet goed
wist wat hier allemaal moest staan.

<br>
