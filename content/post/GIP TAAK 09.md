+++
date = "2016-04-18"
draft = false
title = "INTERACTIEVE JS APPLICATIE"

+++

<div style="text-align: center">
### **TAAK 09 - Interactieve Javascript applicatie**

Als u <a href="http://vincent-boden1.github.io/">hier</a> klikt kan u de interactieve Javascript applicatie bekijken.

Als u <a href="https://gitlab.com/vincent-boden/GipTaakJavascriptBerekeningsApplicaties.git">hier</a> klikt gaat u naar de GitLab repository van deze interactieve Javascript applicatie.

Hier ziet u de README.md van de interactieve Javascript applicatie voor meer uitleg waarover deze applicatie gaat.

### Een Wetenschappelijke website

</div><br>

#### Wat is er te zien

Je zal op een website terecht komen, van hieruit kunt u via het navigatiemenu kiezen tussen 'Berekeningen' en 'simulaties'. Bij de 'Berekeningen' heeft u een overzicht waaruit u kunt kiezen om verschillende wetenschappelijke berekeningen uit te voeren en bij 'simulaties' kunt u verschillende wetenschappelijke simulaties uitvoeren.

##### **Berekeningen**

Bij de 'berekeningen' kunt u gebruik maken van een standaard rekenmachine en u kunt de afstand tussen verschillende planeten berekenen wanneer ze op een rechte lijn zouden liggen.


##### **Simulaties**

Bij de 'simulaties' kunt u enkele simulaties starten zoals de reis door de ruimte waarbij u door te vliegen met een spaceshuttle informatie kunt bekomen van elke planeet in ons zonnestelsel en onze zon. Bij de tabel van Mendeljev vindt u een interactieve tabel van mendeljev waarbij je informatie kunt verzamelen over al de verschillende reeds ontdekte elementen.


#### Wat gebruik ik

Ik maak gebruik van HTML, CSS, Javascript en de Javascript library Jquery.

#### Best Coding Practices

1)  Het gebruiken van commentaar (niet te veel / niet te weinig)

2)   Het kiezen van goede namen voor variabele

3)  Functie namen goed kiezen zodat je meteen weet wat de functie doet

<br>