+++
date = "2016-05-09"
draft = false
title = "OPSTELLING RASPBERRY PI - NFC"

+++

<div style="text-align: center">

### **Raspberry Pi - NFC module Project**

##### **<a href="https://gitlab.com/vincent-boden/RPI-Yorick-Vincent.git">GitLab repository</a>**

</div><br>

##### **Verloop van het project**

Wij, Vincent en Yorick, zijn thuis in onze vrijetijd aan het experimenteren met de Raspberry Pi, dus we wilde hiermee iets doen. Deze opdracht was hier een uitstekend moment voor.

Meneer Cauwenberg stelde het NFC-project voor waarmee leerkrachten met hun badge of smartphone een deur kunnen opendoen.

1) Eerst zijn we op zoek gegaan naar modules om nfc-tags te lezen en naar een slot, gemaakt voor Raspberry Pi. Dit hebben we dan gevonden. We kregen de toestemming van de school om deze NFC-module aan te kopen.

2) Vervolgens gingen we opzoek naar libraries om de NFC-tags te lezen. We vonden de nxppy-library voor python. Hiermee kan je nfc-tags lezen en afprinten op de terminal

3) Daarna hebben we er een database aangekoppelt waardoor we er een whitelist is. Heeft de gebruiker toegang, dan verschijnt er "De deur gaat open". Heeft de gebruiker geen toegang, dan verschijnt er "De deur blijft gesloten."

4) Tenslotte hebben we gebruik gemaakt van de gpio-pins om een lampje te laten branden als men toegang heeft. Dit wordt later vervangen door het slot.

##### **TODO + problemen**

Sommige smartphones hebben een statisch nfc-uid en andere hebben steeds een verschillend uid die random gegenereerd wordt. Iphone's hebben geen nfc. -> Dit leid tot het probleem dat we niet alle smartphones kunnen gebruiken.

Als het slot is besteld, moeten we dit nog aansluiten en testen.

##### **Instructie filmpje**

Hieronder ziet u een instructie filmpje waarin de hardware matige werking van de Raspberry Pi met de NFC module en de NFC apparaten in zijn werking gaat.

<div style="text-align: center">
    <iframe style="width: 75%; height: 500px;" src="https://www.youtube.com/embed/BQMOd7XQyXY" frameborder="0" allowfullscreen></iframe>
</div><br>

##### **Teamwork**

Aan dit project werkt ik samen met <a href="http://disite.be/yorick.scheyltjens/index.php?id=nfc">Yorick Scheyltjens</a>. Ik ken hem al ruim 6 jaar en ik kan ontzettend goed met hem samen werken. Daarom werk ik met hem aan dit project. We hebben beide idëen en vullen elkaar zo aan.