+++
date = "2016-06-06"
draft = false
title = "IT-PROFILE"
+++

<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <style>
    h1 {
        font-size: 4em;
        text-align: center;
    }
    .green {
        background-color: green;
        }
    .red {
        background-color: red;
    }
    .orange {
        background-color: orange;
    }
    .div1 {
        position: relative;
        height: 20px;
    }
    h2 {
        padding-top: 25px;
    }
    @media screen and (max-width : 750px){ 
        .container {
            width: 100%;
        }
    }
  </style>
</head>
<body>

<h1>IT-profile</h1>
<h2>HTML:</h2>
<h3>Begin 6e jaar</h3>
<div class="div1 progress">
    <div class="div2 progress-bar progress-bar-striped orange active" id="HTML-Start"></div>
</div>
<h3>Einde 6e jaar</h3>
<div class="div1 progress">
    <div class="div2 progress-bar progress-bar-striped green active" id="HTML-Einde"></div>
</div>
<h3>Redenen:</h3>
<table>
    <tr><td>1.</td><td>Lessen aan de Karel de Grote hogeschool in Antwerpen</td></tr>
    <tr><td>2.</td><td>De vele gip taken rond de website op onze school</td></tr>
    <tr><td>3.</td><td>Zelf thuis bijgeleerd</td></tr>
</table>

<h2>CSS:</h2>
<h3>Begin 6e jaar</h3>
<div class="div1 progress">
    <div class="div2 progress-bar progress-bar-striped orange active" id="CSS-Start"></div>
</div>
<h3>Einde 6e jaar</h3>
<div class="div1 progress">
    <div class="div2 progress-bar progress-bar-striped green active" id="CSS-Einde"></div>
</div>
<h3>Redenen:</h3>
<table>
    <tr><td>1.</td><td>Lessen aan de Karel de Grote hogeschool in Antwerpen</td></tr>
    <tr><td>2.</td><td>De vele gip taken rond de website op onze school</td></tr>
    <tr><td>3.</td><td>Zelf thuis bijgeleerd</td></tr>
</table>

<h2>Javascript:</h2>
<h3>Begin 6e jaar</h3>
<div class="div1 progress">
    <div class="div2 progress-bar progress-bar-striped red active" id="Javascript-Start"></div>
</div>
<h3>Einde 6e jaar</h3>
<div class="div1 progress">
    <div class="div2 progress-bar progress-bar-striped orange active" id="Javascript-Einde"></div>
</div>
<h3>Redenen:</h3>
<table>
    <tr><td>1.</td><td>Lessen aan de Karel de Grote hogeschool in Antwerpen</td></tr>
    <tr><td>2.</td><td>De vele gip taken rond de website op onze school</td></tr>
    <tr><td>3.</td><td>Zelf thuis bijgeleerd</td></tr>
</table>

<h2>Jquery:</h2>
<h3>Begin 6e jaar</h3>
<div class="div1 progress">
    <div class="div2 progress-bar progress-bar-striped orange active" id="Jquery-Start"></div>
</div>
<h3>Einde 6e jaar</h3>
<div class="div1 progress">
    <div class="div2 progress-bar progress-bar-striped green active" id="Jquery-Einde"></div>
</div>
<h3>Redenen:</h3>
<table>
    <tr><td>1.</td><td>Lessen aan de Karel de Grote hogeschool in Antwerpen</td></tr>
    <tr><td>2.</td><td>De vele gip taken rond de website op onze school</td></tr>
    <tr><td>3.</td><td>Zelf thuis bijgeleerd</td></tr>
</table>

<h2>PHP:</h2>
<h3>Begin 6e jaar</h3>
<div class="div1 progress">
    <div class="div2 progress-bar progress-bar-striped red active" id="PHP-Start"></div>
</div>
<h3>Einde 6e jaar</h3>
<div class="div1 progress">
    <div class="div2 progress-bar progress-bar-striped red active" id="PHP-Einde"></div>
</div>
<table>
    <tr><td>1.</td><td>De periode die we in de lessen eraan besteet hebben (The Wall + oefeningen)</td></tr>
    <tr><td>2.</td><td>Thuis gebruik om oefeningen op te lossen</td></tr>
</table>

<h2>SQL:</h2>
<h3>Begin 6e jaar</h3>
<div class="div1 progress">
    <div class="div2 progress-bar progress-bar-striped green active" id="SQL-Start"></div>
</div>
<h3>Einde 6e jaar</h3>
<div class="div1 progress">
    <div class="div2 progress-bar progress-bar-striped green active" id="SQL-Einde"></div>
</div>
<table>
    <tr><td>1.</td><td>De GIP taak rond het creëren van een database met MySql</td></tr>
    <tr><td>2.</td><td>Het creëren van een databse voor de whitelist van <a href="../GIP TAAK 10">het Raspberry Pi - NFC module project</a></td></tr>
</table>

<h2>Python:</h2>
<h3>Begin 6e jaar</h3>
<div class="div1 progress">
    <div class="div2 progress-bar progress-bar-striped red active" id="Python-Start"></div>
</div>
<h3>Einde 6e jaar</h3>
<div class="div1 progress">
    <div class="div2 progress-bar progress-bar-striped red active" id="Python-Einde"></div>
</div>
<table>
    <tr><td>1.</td><td>Het gebruik van een Raspberry Pi, testen van bepaalde delen door python bestandjes</td></tr>
    <tr><td>2.</td><td>Het creëren van een databse voor de whitelist van <a href="../GIP TAAK 10">het Raspberry Pi - NFC module project</a></td></tr>
</table>

<h2>C#:</h2>
<h3>Begin 6e jaar</h3>
<div class="div1 progress">
    <div class="div2 progress-bar progress-bar-striped orange active" id="C#-Start"></div>
</div>
<h3>Einde 6e jaar</h3>
<div class="div1 progress">
    <div class="div2 progress-bar progress-bar-striped green active" id="C#-Einde"></div>
</div>
<table>
    <tr><td>1.</td><td>De lessen die we op school eraan besteed hebben</td></tr>
</table>

<h2>XAML:</h2>
<h3>Begin 6e jaar</h3>
<div class="div1 progress">
    <div class="div2 progress-bar progress-bar-striped red active" id="XAML-Start"></div>
</div>
<h3>Einde 6e jaar</h3>
<div class="div1 progress">
    <div class="div2 progress-bar progress-bar-striped orange active" id="XAML-Einde"></div>
</div>
<table>
    <tr><td>1.</td><td>De lessen die we op school eraan besteed hebben</td></tr>
</table>

<h2>Cisco:</h2>
<h3>Begin 6e jaar</h3>
<div class="div1 progress">
    <div class="div2 progress-bar progress-bar-striped red active" id="Cisco-Start"></div>
</div>
<h3>Einde 6e jaar</h3>
<div class="div1 progress">
    <div class="div2 progress-bar progress-bar-striped orange active" id="Cisco-Einde"></div>
</div>
<table>
    <tr><td>1.</td><td>De lessen die we op school eraan besteed hebben</td></tr>
    <tr><td>2.</td><td>Kort mee in contact gekomen op mijn stagebedrijf</td></tr>
</table>

<h2>Windows Servers:</h2>
<h3>Begin 6e jaar</h3>
<div class="div1 progress">
    <div class="div2 progress-bar progress-bar-striped orange active" id="WindowsServers-Start"></div>
</div>
<h3>Einde 6e jaar</h3>
<div class="div1 progress">
    <div class="div2 progress-bar progress-bar-striped green active" id="WindowsServers-Einde"></div>
</div>
<table>
    <tr><td>1.</td><td>De lessen die we op school eraan besteed hebben</td></tr>
    <tr><td>2.</td><td>Thuis heel veel geoefend voor de examens</td></tr>
</table>

<h2>Algernon:</h2>
<h3>Begin 6e jaar</h3>
<div class="div1 progress">
    <div class="div2 progress-bar progress-bar-striped red active" id="Algernon-Start"></div>
</div>
<h3>Einde 6e jaar</h3>
<div class="div1 progress">
    <div class="div2 progress-bar progress-bar-striped red active" id="Algernon-Einde"></div>
</div>
<table>
    <tr><td>1.</td><td>De lessen die we op school eraan besteed hebben</td></tr>
</table>

<h2>Git:</h2>
<h3>Begin 6e jaar</h3>
<div class="div1 progress">
    <div class="div2 progress-bar progress-bar-striped green active" id="Git-Start"></div>
</div>
<h3>Einde 6e jaar</h3>
<div class="div1 progress">
    <div class="div2 progress-bar progress-bar-striped green active" id="Git-Einde"></div>
</div>
<table>
    <tr><td>1.</td><td>Het constante gebruik van git voor elk project</td></tr>
    <tr><td>2.</td><td>Ook voor projecten die niet voor school zijn, zoals KDG project</td></tr>
</table>

<h2>Conclusie:</h2>
<p>Aan enkele programmeertalen is er nog wel wat werk aan maar de meeste zijn al wel oke of goed zelfs.</p>
<p>De lessen over HTML, CSS en Javascript aan de KDG hebben mij veel bijgeleerd.</p>
<p>Het gebruik van Git ligt mij in de hand en gaat zeer vlot.</p>
<p>Windows servers begin ik aardig onder de knie te krijgen.</p>

<br><br>

<p style="text-align: center"><a href="">Bekijk de animatie opnieuw</a></p>

<br>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script type="text/javascript">
window.addEventListener("load",slide);
function slide() {
    var talen = ['HTML-Start','HTML-Einde','CSS-Start','CSS-Einde','Javascript-Start','Javascript-Einde','Jquery-Start','Jquery-Einde','PHP-Start','PHP-Einde','SQL-Start','SQL-Einde','Python-Start','Python-Einde','C#-Start','C#-Einde','XAML-Start','XAML-Einde','Cisco-Start','Cisco-Einde','WindowsServers-Start','WindowsServers-Einde','Algernon-Start','Algernon-Einde','Git-Start','Git-Einde'];
	var score = [50,80,45,75,30,55,45,65,15,35,60,75,10,20,55,65,20,35,30,50,55,75,0,20,65,80];
	var waarde = 0;
	function toon(waarde, talen, score) {
		var element = document.getElementById(talen[waarde]);
		var breedte = 1;
		var id = setInterval(controleermax, 5);
		function controleermax() {
			if (breedte >= score[waarde]) {
				clearInterval(id);
				++waarde;
				if(talen.length != waarde) {
					toon(waarde, talen, score);
				}
			} 
			else {
				breedte++;
				element.style.width = breedte + '%';
				element.innerHTML = element.id + ': ' +breedte + '%';
			}
		}
	}
    toon(waarde, talen, score);
}
</script>

</body>
</html>