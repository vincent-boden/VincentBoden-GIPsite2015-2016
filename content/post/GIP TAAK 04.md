+++
date = "2015-12-14"
draft = false
title = "LA FOIRE AUX QUESTIONS - FAQ"

+++

<style>
    ul li {
        width: 12.5%;
    }
    li {
        width: 100%;
    }
</style>

<div style="text-align: center">
### **LA FOIRE AUX QUESTIONS**
</div>
</br>

#### <b>Il y a des différences entre Office online et Office offline?</b>

Oui, il y a des **différences**. Ainsi, il y a **moins d’options** pour la mise en page. Mais pas de souci, Office online a aussi beaucoup de bonnes options.

</br>

#### <b>Où peut-on utiliser OneDrive?</b>

OneDrive est une application **multiplateforme**. Vous pouvez l’utiliser partout. Il y a des applications pour iOS, Mac, Android, Windows. Vous pouvez y **gérer** tous vos fichiers et plus.

</br>

#### <b>Comment partager des fichiers avec OneDrive for Business?</b>

OneDrive a un **avantage** pour partager des documents. C'est facile à utiliser. Suivez ces étapes:

1. Vous devez cliquer à gauche du nom de votre fichier pour **sélectionner** le fichier que vous voulez partager.

2. Après vous cliquez en haut pour **partager**.

3. Maintenant vous voyez **un menu** avec les options que vous pouvez choisir.

</br>

#### <b>Quels types de documents peut-on télécharger?</b>

Il y a 2 options pour **téléverser** des documents en OneDrive online. Vous pouvez créer de nouveaux documents d'Office comme PowerPoint, Word, Excel, … mais il y a aussi une option pour téléverser des documents de **tous types** de votre ordinateur avec un maximum de 10 GiB.

</br>

#### <b>Une connexion Internet est-elle requise pour Office 365 ?</b>

Non, pour utiliser Office, une connexion Internet **n'est pas nécessaire**. Néanmoins, l'installation exige une connexion. Une connexion offre aussi **plus de fonctions** que l’utilisation sans connexion:

1. Services cloud (Synchronisation avec OneDrive et Office online)

2. E-mail

3. IT Management

Donc, quand vous exécutez des changements lorsque vous êtes déconnecté une synchronisation aura lieu quand vous allez **en ligne**.


</br>

#### <b>Comment peut-on changer une extension Office 365 ?</b>

En Office vous pouvez utiliser **plus que les extensions standard** seulement. Si vous voulez adapter l'extension en PDF dans Word par exemple, c'est simple par quelques clics. Procédez comme suit:

1. Cliquez sur "*fichier*" au coin supérieur gauche de votre document et après "*enregistrer sous*".

2. Maintenant vous choisissez **la place d’enregistrement** de votre document.

3. Sous le cadre "*enregistrer*" vous voyez le cadre où vous pouvez changer l'extension.

</br>

#### <b>Qu'est-ce que le cloud?</b>

Le cloud est une désignation pour les services basés sur le web. Le cloud est utilisé par **plusieurs applications**. OneDrive est une partie du cloud Office. Dans le cloud, vous pouvez **stocker** des documents, synchroniser avec OneDrive, téléphoner avec Lync etcétéra. Le cloud n'a pas d’interface mais OneDrive apporte certaines fonctions.

</br>

#### <b>Comment pouvez-vous protéger votre document?</b>

Quand vous avez un document secret ou quand personne ne peut adapter votre document, vous pouvez **protéger** votre document:

1. Cliquez sur "*fichier*" au coin supérieur gauche de votre document. Vous voyez un cadre avec le titre "*Protéger le document*".

2. Comme vous cliquez sur ce cadre vous voyez un menu avec les différentes options pour **protéger votre document**.

<div style="text-align: center">
<a class="btn-floating btn-large waves-effect waves-light" href="../../page/FAQ/FAQ Frans">FAQ FR</a>
<a class="btn-floating btn-large waves-effect waves-light" href="../../page/FAQ/FAQ Nederlands">FAQ NE</a>
</div><br>
